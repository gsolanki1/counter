
import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

const Navbar = () => {

  return (
    <AppBar position="static">
      <Toolbar variant="dense">
        <IconButton edge="start" color="inherit" aria-label="menu">
          <ShoppingCartIcon />
        </IconButton>
        <Typography variant="h6" color="inherit">
          G-CART
        </Typography>
      </Toolbar>
    </AppBar>
  )
}

export default Navbar;