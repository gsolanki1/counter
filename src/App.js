
import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import AddBoxIcon from '@material-ui/icons/AddBox';
import RemoveIcon from '@material-ui/icons/Remove';
import { Button } from '@material-ui/core';

class Item extends React.Component {

  render() { 
    const { onDelete, onIncrement, onDecrement, item } = this.props;

    return (
      <div className="item-section">
        <input type="number" placeholder="Items Added" value={item.value} ></input> 
        <span onClick={() => onIncrement(item)} ><AddBoxIcon /></span>
        <span onClick={() => onDecrement(item)} ><RemoveIcon /></span>
        <Button variant="contained" color="secondary" onClick={() => {onDelete(item.key)}}>
          Delete 
        </Button>
      </div>
    )
  };
};
 
class App extends React.Component {
  state= {
    itemTotal: 0,
    items: [
      {key: 0, value: 0},
      {key: 1, value: 0},
      {key: 2, value: 0},
      {key: 3, value: 0},
      {key: 4, value: 0},
    ],
  };    
  
  handleDelete = (key) => {
    const items = this.state.items.filter((item) => item.key !== key);
    
    this.setState({
      items: items
    })
  }

  handleIncrement = (item) => {
    const items = [...this.state.items];
    const index = items.indexOf(item)
    items[index] = {...item}
    items[index].value++;
    this.setState({
      items: items
    })
  } 

  handleDecrement = (item) => {
    const items = [...this.state.items];
    const index = items.indexOf(item)
    items[index] = {...item}
    
    if(items[index].value === 0) {
      return null 
    } 
    items[index].value--
    this.setState({
      items: items
    })
  } 

  render() {  
    var sum = 0;
    const total = this.state.items.forEach((item) => {
      sum = sum + item.value;
      return sum
    })

    return (
      <div className="App">
        <p><Navbar /></p>
        <div className="item-section">  
          <p>Total {sum}</p><br></br>
        </div> 
        <div>
          {this.state.items.map((item) => 
            { return <p><Item 
                          key={item.key} 
                          item={item}
                          onDelete={this.handleDelete} 
                          onIncrement={this.handleIncrement}  
                          onDecrement={this.handleDecrement}            
                        />
                      </p> 
            })
          }
        </div>
      </div>
    );
  };
}

export default App;
